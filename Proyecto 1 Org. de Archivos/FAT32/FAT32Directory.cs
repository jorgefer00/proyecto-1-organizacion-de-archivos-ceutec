﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Proyecto_1_Org.de_Archivos.FAT32
{
    public class FAT32Directory
    {
        List<FAT32DirEntry> ListaDeArchivos;
        int clusterStart = -1;
        int parentCluster = -1;

        public FAT32Directory(int cluster)
        {
            ListaDeArchivos = new List<FAT32DirEntry>();
            clusterStart = cluster;
        }

        public void AddDirectory(FAT32DirEntry entry)
        {
            ListaDeArchivos.Add(entry);
        }

        public int GetClusterStart()
        {
            return clusterStart;
        }

        public int GetParentCluster()
        {
            return parentCluster;
        }

        public void ParseAllEntries()
        {
            foreach(FAT32DirEntry entry in ListaDeArchivos)
            {
                entry.ParseDirEntry();
                if (entry.isPointingToParent)
                    parentCluster = entry.ParentCluster;
            }
            
        }

        public Int32 NumeroDeArchivos()
        {
            return ListaDeArchivos.Count;
        }

        public List<FAT32DirEntry> GetEntries()
        {
            return ListaDeArchivos;
        }
    }
}
