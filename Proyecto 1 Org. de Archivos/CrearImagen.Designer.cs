﻿namespace Proyecto_1_Org.de_Archivos
{
    partial class CrearImagen
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CrearImagen));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txtVolLabel = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.btnCancelar = new System.Windows.Forms.Button();
            this.btnCrear = new System.Windows.Forms.Button();
            this.checkFormatear = new System.Windows.Forms.CheckBox();
            this.tamanoSectoresPorCluster = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.sizeTypeCombobox = new System.Windows.Forms.ComboBox();
            this.tamanoDisco = new System.Windows.Forms.NumericUpDown();
            this.btnUbicacion = new System.Windows.Forms.Button();
            this.txtRuta = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tamanoDisco)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.txtVolLabel);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.btnCancelar);
            this.groupBox1.Controls.Add(this.btnCrear);
            this.groupBox1.Controls.Add(this.checkFormatear);
            this.groupBox1.Controls.Add(this.tamanoSectoresPorCluster);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.sizeTypeCombobox);
            this.groupBox1.Controls.Add(this.tamanoDisco);
            this.groupBox1.Controls.Add(this.btnUbicacion);
            this.groupBox1.Controls.Add(this.txtRuta);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(13, 13);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(305, 283);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Datos de Disco";
            // 
            // txtVolLabel
            // 
            this.txtVolLabel.Location = new System.Drawing.Point(193, 205);
            this.txtVolLabel.MaxLength = 11;
            this.txtVolLabel.Name = "txtVolLabel";
            this.txtVolLabel.Size = new System.Drawing.Size(106, 20);
            this.txtVolLabel.TabIndex = 16;
            this.txtVolLabel.Text = "SIN NOMBRE ";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(9, 208);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(178, 13);
            this.label4.TabIndex = 15;
            this.label4.Text = "Volume Label (11 Caracteres Max ) :";
            // 
            // btnCancelar
            // 
            this.btnCancelar.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancelar.Location = new System.Drawing.Point(224, 254);
            this.btnCancelar.Name = "btnCancelar";
            this.btnCancelar.Size = new System.Drawing.Size(75, 23);
            this.btnCancelar.TabIndex = 12;
            this.btnCancelar.Text = "Cancelar";
            this.btnCancelar.UseVisualStyleBackColor = true;
            this.btnCancelar.Click += new System.EventHandler(this.btnCancelar_Click);
            // 
            // btnCrear
            // 
            this.btnCrear.Location = new System.Drawing.Point(143, 254);
            this.btnCrear.Name = "btnCrear";
            this.btnCrear.Size = new System.Drawing.Size(75, 23);
            this.btnCrear.TabIndex = 11;
            this.btnCrear.Text = "Crear";
            this.btnCrear.UseVisualStyleBackColor = true;
            this.btnCrear.Click += new System.EventHandler(this.btnCrear_Click);
            // 
            // checkFormatear
            // 
            this.checkFormatear.AutoSize = true;
            this.checkFormatear.Checked = true;
            this.checkFormatear.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkFormatear.Location = new System.Drawing.Point(10, 161);
            this.checkFormatear.Name = "checkFormatear";
            this.checkFormatear.Size = new System.Drawing.Size(125, 17);
            this.checkFormatear.TabIndex = 10;
            this.checkFormatear.Text = "Dar Formato FAT32?";
            this.checkFormatear.UseVisualStyleBackColor = true;
            this.checkFormatear.CheckedChanged += new System.EventHandler(this.checkFormatear_CheckedChanged);
            // 
            // tamanoSectoresPorCluster
            // 
            this.tamanoSectoresPorCluster.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.tamanoSectoresPorCluster.FormattingEnabled = true;
            this.tamanoSectoresPorCluster.Items.AddRange(new object[] {
            "512 bytes",
            "1 KiB",
            "2 KiB",
            "4 KiB",
            "8 KiB",
            "16 KiB"});
            this.tamanoSectoresPorCluster.Location = new System.Drawing.Point(159, 178);
            this.tamanoSectoresPorCluster.Name = "tamanoSectoresPorCluster";
            this.tamanoSectoresPorCluster.Size = new System.Drawing.Size(140, 21);
            this.tamanoSectoresPorCluster.TabIndex = 9;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(7, 181);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(140, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Tamano de Cluster (Bytes)  :";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(56, 95);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(97, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Tamano de Disco :";
            // 
            // sizeTypeCombobox
            // 
            this.sizeTypeCombobox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.sizeTypeCombobox.FormattingEnabled = true;
            this.sizeTypeCombobox.Items.AddRange(new object[] {
            "MB",
            "GB"});
            this.sizeTypeCombobox.Location = new System.Drawing.Point(245, 92);
            this.sizeTypeCombobox.Name = "sizeTypeCombobox";
            this.sizeTypeCombobox.Size = new System.Drawing.Size(54, 21);
            this.sizeTypeCombobox.TabIndex = 4;
            this.sizeTypeCombobox.SelectedIndexChanged += new System.EventHandler(this.sizeTypeCombobox_SelectedIndexChanged);
            // 
            // tamanoDisco
            // 
            this.tamanoDisco.Location = new System.Drawing.Point(159, 93);
            this.tamanoDisco.Maximum = new decimal(new int[] {
            999,
            0,
            0,
            0});
            this.tamanoDisco.Name = "tamanoDisco";
            this.tamanoDisco.Size = new System.Drawing.Size(82, 20);
            this.tamanoDisco.TabIndex = 3;
            this.tamanoDisco.ValueChanged += new System.EventHandler(this.tamanoDisco_ValueChanged);
            // 
            // btnUbicacion
            // 
            this.btnUbicacion.Location = new System.Drawing.Point(170, 63);
            this.btnUbicacion.Name = "btnUbicacion";
            this.btnUbicacion.Size = new System.Drawing.Size(129, 23);
            this.btnUbicacion.TabIndex = 2;
            this.btnUbicacion.Text = "Seleccionar ubicacion";
            this.btnUbicacion.UseVisualStyleBackColor = true;
            this.btnUbicacion.Click += new System.EventHandler(this.button1_Click);
            // 
            // txtRuta
            // 
            this.txtRuta.Location = new System.Drawing.Point(10, 37);
            this.txtRuta.Name = "txtRuta";
            this.txtRuta.ReadOnly = true;
            this.txtRuta.Size = new System.Drawing.Size(289, 20);
            this.txtRuta.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(7, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(30, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Ruta";
            // 
            // CrearImagen
            // 
            this.AcceptButton = this.btnCrear;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnCancelar;
            this.ClientSize = new System.Drawing.Size(330, 308);
            this.Controls.Add(this.groupBox1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "CrearImagen";
            this.Text = "Crear Imagen de Disco";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tamanoDisco)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnUbicacion;
        private System.Windows.Forms.TextBox txtRuta;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox sizeTypeCombobox;
        private System.Windows.Forms.NumericUpDown tamanoDisco;
        private System.Windows.Forms.ComboBox tamanoSectoresPorCluster;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.CheckBox checkFormatear;
        private System.Windows.Forms.Button btnCancelar;
        private System.Windows.Forms.Button btnCrear;
        private System.Windows.Forms.TextBox txtVolLabel;
        private System.Windows.Forms.Label label4;
    }
}