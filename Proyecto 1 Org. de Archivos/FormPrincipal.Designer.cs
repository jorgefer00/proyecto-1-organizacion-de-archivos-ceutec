﻿namespace Proyecto_1_Org.de_Archivos
{
    partial class FormPrincipal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormPrincipal));
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.archivoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.abrirImagenToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.crearImagenToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cerrarImagenToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.salirToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.opcionesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.formatearToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.informacionDeDiscoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.DirectoryTree = new System.Windows.Forms.TreeView();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.label1 = new System.Windows.Forms.Label();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripLabel1 = new System.Windows.Forms.ToolStripLabel();
            this.toolStripSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.FileListView = new System.Windows.Forms.ListView();
            this.nombreArchivo = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.tipo = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.tamanoArchivo = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.creado = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.modificado = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.cluster = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.numeroDeArchivosLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.statusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.freeSpaceAvailable = new System.Windows.Forms.ToolStripStatusLabel();
            this.previosFolderButton = new System.Windows.Forms.ToolStripButton();
            this.saveToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.toolStripUploadButton = new System.Windows.Forms.ToolStripButton();
            this.newFolderButton = new System.Windows.Forms.ToolStripButton();
            this.helpToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.toolStrip1.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.archivoToolStripMenuItem,
            this.opcionesToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1088, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // archivoToolStripMenuItem
            // 
            this.archivoToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.abrirImagenToolStripMenuItem,
            this.crearImagenToolStripMenuItem,
            this.cerrarImagenToolStripMenuItem,
            this.salirToolStripMenuItem});
            this.archivoToolStripMenuItem.Name = "archivoToolStripMenuItem";
            this.archivoToolStripMenuItem.Size = new System.Drawing.Size(60, 20);
            this.archivoToolStripMenuItem.Text = "Archivo";
            // 
            // abrirImagenToolStripMenuItem
            // 
            this.abrirImagenToolStripMenuItem.Name = "abrirImagenToolStripMenuItem";
            this.abrirImagenToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.O)));
            this.abrirImagenToolStripMenuItem.Size = new System.Drawing.Size(198, 22);
            this.abrirImagenToolStripMenuItem.Text = "Abrir imagen....";
            this.abrirImagenToolStripMenuItem.Click += new System.EventHandler(this.abrirImagenToolStripMenuItem_Click);
            // 
            // crearImagenToolStripMenuItem
            // 
            this.crearImagenToolStripMenuItem.Name = "crearImagenToolStripMenuItem";
            this.crearImagenToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.N)));
            this.crearImagenToolStripMenuItem.Size = new System.Drawing.Size(198, 22);
            this.crearImagenToolStripMenuItem.Text = "Crear imagen...";
            this.crearImagenToolStripMenuItem.Click += new System.EventHandler(this.crearImagenToolStripMenuItem_Click);
            // 
            // cerrarImagenToolStripMenuItem
            // 
            this.cerrarImagenToolStripMenuItem.Enabled = false;
            this.cerrarImagenToolStripMenuItem.Name = "cerrarImagenToolStripMenuItem";
            this.cerrarImagenToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.K)));
            this.cerrarImagenToolStripMenuItem.Size = new System.Drawing.Size(198, 22);
            this.cerrarImagenToolStripMenuItem.Text = "Cerrar Imagen";
            this.cerrarImagenToolStripMenuItem.Click += new System.EventHandler(this.cerrarImagenToolStripMenuItem_Click);
            // 
            // salirToolStripMenuItem
            // 
            this.salirToolStripMenuItem.Name = "salirToolStripMenuItem";
            this.salirToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.F4)));
            this.salirToolStripMenuItem.Size = new System.Drawing.Size(198, 22);
            this.salirToolStripMenuItem.Text = "Salir";
            this.salirToolStripMenuItem.Click += new System.EventHandler(this.salirToolStripMenuItem_Click);
            // 
            // opcionesToolStripMenuItem
            // 
            this.opcionesToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.formatearToolStripMenuItem,
            this.informacionDeDiscoToolStripMenuItem});
            this.opcionesToolStripMenuItem.Name = "opcionesToolStripMenuItem";
            this.opcionesToolStripMenuItem.Size = new System.Drawing.Size(69, 20);
            this.opcionesToolStripMenuItem.Text = "Opciones";
            // 
            // formatearToolStripMenuItem
            // 
            this.formatearToolStripMenuItem.Enabled = false;
            this.formatearToolStripMenuItem.Name = "formatearToolStripMenuItem";
            this.formatearToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.F)));
            this.formatearToolStripMenuItem.Size = new System.Drawing.Size(224, 22);
            this.formatearToolStripMenuItem.Text = "Formatear";
            this.formatearToolStripMenuItem.Click += new System.EventHandler(this.formatearToolStripMenuItem_Click);
            // 
            // informacionDeDiscoToolStripMenuItem
            // 
            this.informacionDeDiscoToolStripMenuItem.Enabled = false;
            this.informacionDeDiscoToolStripMenuItem.Name = "informacionDeDiscoToolStripMenuItem";
            this.informacionDeDiscoToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.I)));
            this.informacionDeDiscoToolStripMenuItem.Size = new System.Drawing.Size(224, 22);
            this.informacionDeDiscoToolStripMenuItem.Text = "Informacion de Disco";
            this.informacionDeDiscoToolStripMenuItem.Click += new System.EventHandler(this.informacionDeDiscoToolStripMenuItem_Click);
            // 
            // DirectoryTree
            // 
            this.DirectoryTree.ImageIndex = 0;
            this.DirectoryTree.ImageList = this.imageList1;
            this.DirectoryTree.Location = new System.Drawing.Point(3, 28);
            this.DirectoryTree.Name = "DirectoryTree";
            this.DirectoryTree.SelectedImageIndex = 0;
            this.DirectoryTree.Size = new System.Drawing.Size(304, 368);
            this.DirectoryTree.TabIndex = 1;
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "harddisk.png");
            this.imageList1.Images.SetKeyName(1, "folderclose.png");
            this.imageList1.Images.SetKeyName(2, "folderopen.png");
            this.imageList1.Images.SetKeyName(3, "file.png");
            // 
            // splitContainer1
            // 
            this.splitContainer1.Location = new System.Drawing.Point(13, 27);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.label1);
            this.splitContainer1.Panel1.Controls.Add(this.DirectoryTree);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.toolStrip1);
            this.splitContainer1.Panel2.Controls.Add(this.FileListView);
            this.splitContainer1.Size = new System.Drawing.Size(1063, 399);
            this.splitContainer1.SplitterDistance = 310;
            this.splitContainer1.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 12);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(49, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Carpetas";
            // 
            // toolStrip1
            // 
            this.toolStrip1.Enabled = false;
            this.toolStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.previosFolderButton,
            this.toolStripSeparator3,
            this.toolStripLabel1,
            this.saveToolStripButton,
            this.toolStripSeparator,
            this.toolStripUploadButton,
            this.toolStripSeparator2,
            this.newFolderButton,
            this.toolStripSeparator1,
            this.helpToolStripButton});
            this.toolStrip1.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.HorizontalStackWithOverflow;
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(749, 27);
            this.toolStrip1.TabIndex = 2;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(6, 27);
            // 
            // toolStripLabel1
            // 
            this.toolStripLabel1.Name = "toolStripLabel1";
            this.toolStripLabel1.Size = new System.Drawing.Size(157, 24);
            this.toolStripLabel1.Text = "Operaciones de Archivos -> ";
            // 
            // toolStripSeparator
            // 
            this.toolStripSeparator.Name = "toolStripSeparator";
            this.toolStripSeparator.Size = new System.Drawing.Size(6, 27);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 27);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 27);
            // 
            // FileListView
            // 
            this.FileListView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.nombreArchivo,
            this.tipo,
            this.tamanoArchivo,
            this.creado,
            this.modificado,
            this.cluster});
            this.FileListView.FullRowSelect = true;
            this.FileListView.GridLines = true;
            this.FileListView.LargeImageList = this.imageList1;
            this.FileListView.Location = new System.Drawing.Point(3, 28);
            this.FileListView.Name = "FileListView";
            this.FileListView.Size = new System.Drawing.Size(743, 368);
            this.FileListView.SmallImageList = this.imageList1;
            this.FileListView.TabIndex = 0;
            this.FileListView.UseCompatibleStateImageBehavior = false;
            this.FileListView.View = System.Windows.Forms.View.Details;
            this.FileListView.DoubleClick += new System.EventHandler(this.FileListView_DoubleClick);
            // 
            // nombreArchivo
            // 
            this.nombreArchivo.Text = "Nombre";
            this.nombreArchivo.Width = 150;
            // 
            // tipo
            // 
            this.tipo.Text = "Tipo";
            this.tipo.Width = 96;
            // 
            // tamanoArchivo
            // 
            this.tamanoArchivo.Text = "Tamaño (Bytes)";
            this.tamanoArchivo.Width = 100;
            // 
            // creado
            // 
            this.creado.Text = "Fecha de Creacion";
            this.creado.Width = 150;
            // 
            // modificado
            // 
            this.modificado.Text = "Fecha de Modificacion";
            this.modificado.Width = 150;
            // 
            // cluster
            // 
            this.cluster.Text = "Primer # Cluster";
            this.cluster.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.cluster.Width = 100;
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1,
            this.numeroDeArchivosLabel,
            this.statusLabel,
            this.freeSpaceAvailable});
            this.statusStrip1.Location = new System.Drawing.Point(0, 429);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(1088, 22);
            this.statusStrip1.TabIndex = 3;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(125, 17);
            this.toolStripStatusLabel1.Text = "Numero de Archivos : ";
            // 
            // numeroDeArchivosLabel
            // 
            this.numeroDeArchivosLabel.Name = "numeroDeArchivosLabel";
            this.numeroDeArchivosLabel.Size = new System.Drawing.Size(12, 17);
            this.numeroDeArchivosLabel.Text = "-";
            // 
            // statusLabel
            // 
            this.statusLabel.Name = "statusLabel";
            this.statusLabel.Size = new System.Drawing.Size(127, 17);
            this.statusLabel.Text = "Espacio Libre (Aprox) : ";
            // 
            // freeSpaceAvailable
            // 
            this.freeSpaceAvailable.Name = "freeSpaceAvailable";
            this.freeSpaceAvailable.Size = new System.Drawing.Size(43, 17);
            this.freeSpaceAvailable.Text = "- bytes";
            // 
            // previosFolderButton
            // 
            this.previosFolderButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.previosFolderButton.Image = global::Proyecto_1_Org.de_Archivos.Properties.Resources.undo;
            this.previosFolderButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.previosFolderButton.Name = "previosFolderButton";
            this.previosFolderButton.Size = new System.Drawing.Size(24, 24);
            this.previosFolderButton.Text = "Ir al padre...";
            // 
            // saveToolStripButton
            // 
            this.saveToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.saveToolStripButton.Image = global::Proyecto_1_Org.de_Archivos.Properties.Resources.page_down_icon;
            this.saveToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.saveToolStripButton.Name = "saveToolStripButton";
            this.saveToolStripButton.Size = new System.Drawing.Size(24, 24);
            this.saveToolStripButton.Text = "Guardar Archivo";
            this.saveToolStripButton.Click += new System.EventHandler(this.saveToolStripButton_Click);
            // 
            // toolStripUploadButton
            // 
            this.toolStripUploadButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripUploadButton.Image = global::Proyecto_1_Org.de_Archivos.Properties.Resources.page_up_icon;
            this.toolStripUploadButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripUploadButton.Name = "toolStripUploadButton";
            this.toolStripUploadButton.Size = new System.Drawing.Size(24, 24);
            this.toolStripUploadButton.Text = "Insertar Archivo";
            // 
            // newFolderButton
            // 
            this.newFolderButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.newFolderButton.Image = global::Proyecto_1_Org.de_Archivos.Properties.Resources.Folder_new_icon;
            this.newFolderButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.newFolderButton.Name = "newFolderButton";
            this.newFolderButton.Size = new System.Drawing.Size(24, 24);
            this.newFolderButton.Text = "Nuevo Folder";
            // 
            // helpToolStripButton
            // 
            this.helpToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.helpToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("helpToolStripButton.Image")));
            this.helpToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.helpToolStripButton.Name = "helpToolStripButton";
            this.helpToolStripButton.Size = new System.Drawing.Size(24, 24);
            this.helpToolStripButton.Text = "Informacion de Archivo";
            this.helpToolStripButton.Click += new System.EventHandler(this.helpToolStripButton_Click);
            // 
            // FormPrincipal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.ClientSize = new System.Drawing.Size(1088, 451);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.menuStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.MaximizeBox = false;
            this.Name = "FormPrincipal";
            this.Text = "Proyecto 1 Org. de Archivos - Jorge Fernandez";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel1.PerformLayout();
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem archivoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem abrirImagenToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem crearImagenToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cerrarImagenToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem salirToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem opcionesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem formatearToolStripMenuItem;
        private System.Windows.Forms.TreeView DirectoryTree;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.ListView FileListView;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ToolStripMenuItem informacionDeDiscoToolStripMenuItem;
        private System.Windows.Forms.ImageList imageList1;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.ToolStripStatusLabel numeroDeArchivosLabel;
        private System.Windows.Forms.ColumnHeader nombreArchivo;
        private System.Windows.Forms.ColumnHeader tipo;
        private System.Windows.Forms.ColumnHeader tamanoArchivo;
        private System.Windows.Forms.ColumnHeader creado;
        private System.Windows.Forms.ColumnHeader modificado;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripLabel toolStripLabel1;
        private System.Windows.Forms.ToolStripButton saveToolStripButton;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripButton helpToolStripButton;
        private System.Windows.Forms.ColumnHeader cluster;
        private System.Windows.Forms.ToolStripStatusLabel statusLabel;
        private System.Windows.Forms.ToolStripButton toolStripUploadButton;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripButton newFolderButton;
        private System.Windows.Forms.ToolStripButton previosFolderButton;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripStatusLabel freeSpaceAvailable;
    }
}

