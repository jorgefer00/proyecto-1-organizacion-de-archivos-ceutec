﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Proyecto_1_Org.de_Archivos.FAT32;
using Proyecto_1_Org.de_Archivos.Custom_Views;


namespace Proyecto_1_Org.de_Archivos
{
    public partial class FileInfoForm : Form
    {
        public FileInfoForm(FAT32DirEntry entry)
        {
            InitializeComponent();

            this.MaximumSize = this.MinimumSize = this.Size;
            
            labelNombre.Text = entry.NombreDeDir;
            labelFechaCreacion.Text = entry.GetFechaCreacionString();
            labelFechaMod.Text = entry.GetFechaModificacionString();
            labelTamano.Text = entry.GetDirSizeString() + " bytes";
            labelCluster.Text = "" + entry.GetFirstCluster();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
